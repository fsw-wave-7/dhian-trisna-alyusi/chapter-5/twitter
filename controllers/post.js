const { successResponse } = require('../helpers/response.js')

class Post {
    constructor() {
        this.post = []
    }

    getPost = (req, res) => {
        successResponse(
            res,
            200,
            this.post,
            { total: this.post.length }
        )
    }

    getDetailPost = (req, res) => {
        const index = req.params.index
        successResponse(res, 200, this.post[index])
    }

    insertPost = (req, res) => {
        const body = req.body

        const param = {
            'text': body.text,
            'created_at': new Date(),
            'username': body.username 
        }

        this.post.push(param)
        successResponse(res, 201, param)
    }

    updatePost = (req, res) => {
        const index = req.params.index
        const body = req.body

        this.post[index].text = body.text
        this.post[index].username = body.username
        this.post[index].created_at = body.created_at

        successResponse(res, 200, this.post[index])
    }

    deletePost = (req, res) => {
        const index = req.params.index
        this.post.splice(index, 1);
        successResponse(res, 200, null)
    }
}

module.exports = Post


// create, READ, UPDATE, delete
// CRUD