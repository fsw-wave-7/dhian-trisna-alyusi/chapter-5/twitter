const express = require('express')
const morgan = require('morgan')

const path = require('path')

const api = require('./routes/api.js')

const app = express()
const port = 3000

// Set view engine
app.set('view engine', 'ejs')

// Load static files
app.use(express.static(__dirname + '/public'))

// Third Party Middleware for Logging
app.use(morgan('dev'))

// Load api routes
app.use('/api', api)

app.get('/menu', function(req, res) {
    res.render(path.join(__dirname, './views/menu'))
})

app.get('/', function(req, res) {
    const name = req.query.name || 'Starbucks'
    res.render(path.join(__dirname, './views/index'), {
        name: name
    })
})

// Internal Server Error Handler Middleware
app.use(function(err, req, res, next) {
    res.status(500).json({
        status: 'fail',
        errors: err.message
    })
})

// 404 Handler Middleware
app.use(function(req, res, next) {
    res.status(404).json({
        status: 'fail',
        errors: 'Are you lost?'
    })
})

app.listen(port, () => { console.log("Server berhasil dijalankan!!") })
